

 <title>Professor Larry M. Bartels Personal Website</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="sarah" content="">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="img/bartels-larry.jpg">

<!--CSS styles-->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/font-awesome.min.css">  
<link rel="stylesheet" href="css/perfect-scrollbar-0.4.5.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">

<link rel="stylesheet" href="css/highcharts.css">

<link rel="stylesheet" href="css/style.css">
<link id="theme-style" rel="stylesheet" href="css/styles/cyan.css">
<link rel='stylesheet' type='text/css' href='css/menu.css' />

<!-- <link rel="stylesheet" type="text/css" href="css/social.css" />
<link rel="stylesheet" type="text/css" href="css/citation.css" /> -->



<!--/CSS styles-->



<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script src="../../ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script src="js/socialbars.js" type="text/javascript"></script>

<script type="text/javascript" src="js/highcharts.js"></script>



<script type="text/javascript" src="js/TweenMax.min.js"></script>
<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>

<script type="text/javascript" src="js/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="js/jquery.dropdownit.js"></script>

<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
<script type="text/javascript" src="js/ScrollToPlugin.min.js"></script>



<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>

<script type="text/javascript" src="js/masonry.min.js"></script>

<script type="text/javascript" src="js/perfect-scrollbar-0.4.5.with-mousewheel.min.js"></script>

<script type="text/javascript" src="js/magnific-popup.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

<!--/Javascript files 
<script type="text/javascript" src="sliderengine/jquery.js"></script>-->
<script type="text/javascript" src="sliderengine/jquery.hislider.js"></script>



<script type='text/javascript' src='css/menu_jquery.js'></script>

