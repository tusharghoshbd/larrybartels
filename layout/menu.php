

<div id="nav-container">
    <br/>
    <div id="profile" class="">
        <div class="portrate "></div>
        <div class="title">
            <h2>
                <p style="font-size: 16px"><b>Larry M. Bartels</b></p>
                <p> Department of Political Science, Vanderbilt University</p> 
                <p>larry.bartels@vanderbilt.edu</p>
            </h2>
            <div id="social_icons" class="visible-xs">
                <ul>
                    <li><a href="#" target="_blank"><img src="img/linkedin.png" alt="Flickr"></a></li>
                    <li><a href="#" target="_blank"><img src="img/evernote.png" alt="img"></a></li>
                    
                    <li><a href="#" target="_blank"><img src="img/google_plus.png" wid alt="Google+"></a></li>
                    <li><a href="#" target="_blank"><img src="img/facebook.png" alt="Facebook"></a></li>
                    
                </ul>     
            </div>
        </div>
    </div>

    <div id='cssmenu' class="hidden-xs" style="height: 263px; overflow: hidden;">
        <ul >
            <li><a href='index.php'><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
           <!--  <li><a href='profile.php'><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li> -->
           
            <li class='has-sub'><a href='#'><i class="fa fa-user" aria-hidden="true"></i> Profile</a>
                <ul>
                    <li class='last'><a href='about-me.php'>About Me</a></li> 
                    <li class='last'><a href='#'>Awards & Recoginitions </a></li>
                   <!--  <li class='last'><a href='#'>Memberships</a></li>   -->          


                </ul>
            </li>
             <li><a href='publications.php'><i class="fa fa-tasks" aria-hidden="true"></i> Publications</a></li>
            <!-- <li><a href='#'><i class="fa fa-home" aria-hidden="true"></i> Research</a></li>
            <li class='has-sub'><a href='#'><i class="fa fa-home" aria-hidden="true"></i> Contributions</a>
                <ul>
                    <li class='last'><a href='#'><span>Publications</span></a></li>
                    <li class='last'><a href='#'><span>Presentations</span></a></li>              
                    <li><a href='media'><span>Media</span></a></li>

                </ul>
            </li>
            <li class='has-sub'><a href='#'><i class="fa fa-home" aria-hidden="true"></i> Activities</a>
                <ul>
                    <li class='last'><a href='#'><span>Supervisory Activities</span></a></li>
                    <li class='last'><a href='#'><span>Administrative Activities</span></a></li>              


                </ul>
            </li> -->
            <li><a href='work-experience.php'><i class="fa fa-briefcase" aria-hidden="true"></i> Work Experience</a></li>
            <li><a href='education.php'><i class="fa fa-graduation-cap" aria-hidden="true"></i> Education</a></li>


            <li class='last'><a href='contact.php'><i class="fa fa-phone" aria-hidden="true"></i> Contact</a></li>
        </ul>
    </div>
    <div id="social_icons" class="hidden-xs">
        <ul>
            <li><a href="#" target="_blank"><img src="img/linkedin.png" alt="Flickr"></a></li>
            <li><a href="#" target="_blank"><img src="img/evernote.png" alt="img"></a></li>
            
            <li><a href="#" target="_blank"><img src="img/google_plus.png" wid alt="Google+"></a></li>
            <li><a href="#" target="_blank"><img src="img/facebook.png" alt="Facebook"></a></li>
            
        </ul>     
    </div>


    <div class="visible-xs mobile-menu">
    <nav class = "navbar navbar-inverse" role = "navigation">
   
   <div class = "navbar-header">
      

      <ul class="nav navbar-nav navbar-right mobile-bar">
                <li>
                    <a href="index.php">
                        <span class="menu-icon fa fa-home"></span>
                        Home
                    </a>
                </li>
                <li>
                    <a href="about-me.php">
                        <span class="menu-icon fa fa-user"></span>
                        <span class="visible-xs">About me</span>
                    </a>
                </li>
                <li>
                    <a href="contact.php">
                        <span class="menu-icon fa fa-phone"></span>  
                        <span class="visible-xs">Contact</span>
                    </a>
                </li>

                <!-- data-toggle = "collapse" data-target = "#example-navbar-collapse" -->
                <li  >
                    <a href="javascript:void(0)" class="nav-icon">
                        <span class="menu-icon fa fa-bars"></span>
                        More
                    </a>
                </li>
            </ul>
   </div>
   
   <div class = "collapse navbar-collapse" id = "example-navbar-collapse">
    
      <ul class = "nav navbar-nav">
         <li ><a href = "#"><i class="fa fa-trophy"></i> Awards & Recoginitions</a></li>
         <li><a href = "publications.php"><i class="fa fa-tasks" ></i> Publications</a></li>
         <li><a href = "work-experience.php"><i class="fa fa-briefcase" ></i> Work Experience</a></li>
         <li><a href = "education.php"><i class="fa fa-graduation-cap" ></i> Education</a></li>
            
         <!--<li class = "dropdown">
            <a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">
               Java 
               <b class = "caret"></b>
            </a>
            
             <ul class = "dropdown-menu">
               <li><a href = "#">jmeter</a></li>
               <li><a href = "#">EJB</a></li>
               <li><a href = "#">Jasper Report</a></li>
               
               <li class = "divider"></li>
               <li><a href = "#">Separated link</a></li>
               
               <li class = "divider"></li>
               <li><a href = "#">One more separated link</a></li>
            </ul> -->
            
         </li>
            
      </ul>
   </div>
   
</nav>
</div>
</div>