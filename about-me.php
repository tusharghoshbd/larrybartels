<!DOCTYPE html>
<html lang="en">    
    <head>
        
        <?php include_once 'layout/header.php'; ?> 


        <script type="text/javascript">
            $(document).ready(function () {

                if ( $(window).width() <= 767) { 
                  $(window).scroll(function () {
                        if ( $(this).scrollTop() > 323) {
                            $(".mobile-menu").addClass("fixed");
                        }
                        else {
                             $(".mobile-menu").removeClass("fixed");
                        }
                    });
                } 
                else {
                  //Add your javascript for small screens here 
                }

                $('.nav-icon').click(function(){
                    $("#example-navbar-collapse").slideToggle();
                });

                 
                //lineChart();



            });// end of documetn


            



        </script>
    </head>
    <body>
        <div class="container-fluid"><!-- 
            <a href="#sidebar" class="mobilemenu"><i class="icon-reorder"></i></a> -->
            <div class="row no-padding no-margin">
                <div class="col-md-3 col-sm-3 col-xs-12 no-margin no-padding">
                    <div id="main-nav">
                        <?php include_once 'layout/menu.php'; ?> 
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 no-margin no-padding">
                    <div class="pageheader">
                        <div class="headercontent">
                            <div class="section-container">
                                
                                <h2 class="title">PROFILE > ABOUT ME</h2>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                  <p></p>                                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class=" section-container headercontent ">

                     <div class="section color-1"></div>

                        <div class="row" align="left"> 
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h5><b>BIBLIOMETRIC DATA</b></h5>
                                <hr style="background-color:#999; height:1px; border:0px; margin-top:0px;" align="center">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <table class="table table-bordered">
                                    <thead>
                                          <tr>
                                              <td>&nbsp;Total Articles in Publication List</td><td>&nbsp;
                                                                
                                              <a href="http://www.researcherid.com/#" target="_blank"> 
                                              152</a>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>&nbsp;Articles With Citation Data</td>
                                              <td>&nbsp;
                                                    <a href="http://www.researcherid.com/#" target="_blank"> 152</a>
                                              </td>
                                          </tr>
                                          <tr>
                                                <td>&nbsp;Sum of the Times Cited </td>
                                                <td>&nbsp;
                                                          <a href="http://www.researcherid.com/#" target="_blank" > 567</a>
                                          
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>&nbsp;Average Citations per Article </td>
                                                <td>&nbsp;
                                                    <a href="http://www.researcherid.com/#" target="_blank" > 22.21</a>
                                                </td>
                                          </tr>
                                          
                                          <tr>
                                                <td>&nbsp;h-index </td><td>&nbsp;
                                           
                                                             <a href="http://www.researcherid.com/#" target="_blank" > 27</a>
                                                </td>
                                          </tr>
                                              <!-- <tr>
                                                    <td>&nbsp;Co-authors </td>
                                                    <td>&nbsp;
                                                        <a href="http://labs.researcherid.com/#" target="_blank" > Collaboration Network</a>
                                                    </td>
                                              </tr> -->
                                              
                                                <tr>
                                                        <td colspan="2">&nbsp;This information is generated from  
                                                        <a href="http://www.researcherid.com" target="_blank">http://researcherid.com</a>
                                                     </td>
                                              </tr>
                                    </thead>
                                 </table>    
                             </div>
                        </div>



                        <br/><br/>




                         <div class="row" align="left"> 
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h5><b>BIOGRAPHY</b></h5>
                                <hr style="background-color:#999; height:1px; border:0px; margin-top:0px;" align="center">
                            </div>
                        </div>
                         <div class="row" align="left"> 
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <p class="text-justify">
                                    Bartels holds the May Werthan Shayne Chair of Public Policy and Social Science at Vanderbilt University. His scholarship and teaching focus broadly on American democracy, including public opinion, electoral politics, public policy, and political representation. 
                                </p>
                                <p class="text-justify">
                                    His books include Unequal Democracy: The Political Economy of the New Gilded Age; Democracy for Realists: Why Elections Do Not Produce Responsive Government (with Christopher Achen); Presidential Primaries and the Dynamics of Public Choice; Mass Politics in Tough Times: Opinions, Votes, and Protest in the Great Recession (co-edited with Nancy Bermeo); and Campaign Reform: Insights and Evidence (co-edited with Lynn Vavreck). He is also the author of numerous scholarly articles and of occasional pieces in the New York Times, Washington Post, Los Angeles Times, Salon, and other media outlets.  Bartels was educated at Yale University (B.A., M.A.) and the University of California, Berkeley (Ph.D.). 
                                </p>
                                <p class="text-justify">
                                    He joined the Vanderbilt faculty in 2011 following stints at the University of Rochester (1983-1991) and Princeton University (1991-2011). He is a trustee of the Russell Sage Foundation, co-chair of the Social Science Research Council’s Working Group on Political Participation, and co-director of Vanderbilt’s Center for the Study of Democratic Institutions. He has previously served as vice president of the American Political Science Association and president of its Political Methodology section, chair of the Board.
                        </div>
                        <div class="section color-1"></div>
                    </div>
                </div>
            </div>  
        </div>
    </body>
</html>