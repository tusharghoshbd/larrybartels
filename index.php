<?php  
       // require 'client-history.php';  client_history("larrybartels/index.php")  
?>

<!DOCTYPE html>
<html lang="en">    
    <head>
       
        
        <?php include_once 'layout/header.php'; ?> 

        <style type="text/css">


        </style>
        <script type="text/javascript">
            $(document).ready(function () {

                if ( $(window).width() <= 767) { 
                  $(window).scroll(function () {
                        if ( $(this).scrollTop() > 323) {
                            $(".mobile-menu").addClass("fixed");
                        }
                        else {
                             $(".mobile-menu").removeClass("fixed");
                        }
                    });
                } 
                else {
                  //Add your javascript for small screens here 
                }

                $('.nav-icon').click(function(){
                    $("#example-navbar-collapse").slideToggle();
                });

                 
                lineChart();



            });// end of documetn


            function lineChart(){
                $("#container").highcharts(
{
    chart: {
        type: 'column'
    },
    title: {
        text: 'Annual Publications by Year'
    },
    xAxis: {
        categories: [2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Publications'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
                        name: 'Journal',
                        data: [0, 28, 25, 20, 28, 28, 47, 79,72,45,118,190,198, 115, 0]
                    }, {
                        name: 'SCOPUS',
                        data: [0, 25, 141, 164, 130, 255, 240, 230,242,256,239,230,120,149,58]
                    }, {
                        name: 'Other Indexing',
                        data: [25,60, 40,25,160,180,176, 100,262,241,205,170,110,58,0]
                    }, {
                        name: 'Book',
                        data: [0, 0, 10, 19, 11, 2,22,2,9,6,1,5,17, 0, 27]
                    }, {
                        name: 'Chapter in Book',
                        data: [8, 8, 5, 18, 9, 16, 4, 1,0,2,41,5,9,7,2]
                    }]
}


                );//end of chart container
            }



        </script>
    </head>
    <body>
        <div class="container-fluid"><!-- 
            <a href="#sidebar" class="mobilemenu"><i class="icon-reorder"></i></a> -->
            <div class="row no-padding no-margin">
                <div class="col-md-3 col-sm-3 col-xs-12 no-margin no-padding">
                    <div id="main-nav">
                        <?php include_once 'layout/menu.php'; ?> 
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 no-margin no-padding">
                    <div class="pageheader">
                        <div class="headercontent">
                            <div class="section-container">
                                
                                <h2 class="title">HOME</h2>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                  <p></p>                                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class=" section-container headercontent ">
                     <!--    <marquee style=" margin-bottom:5px; margin-top:5px; direction="left" scrollamount="2" bgcolor="#00cccc"loop="-1" >   <center>  <h4 class><blink>Welcome To Professor Mohamed Cheriet personal Web page</blink> </h4></center> </marquee> -->
                        <div class="section color-1"></div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default ">
                                        <span>Publication Statistics</span>
                                    </div>
                                    <div class="panel-body">
                                    <div class="table-responsive">
                                        <div id="container"  style="height: 300px; min-width: 400px"></div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Start Widget Project -->
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default">
                                        <span>Last 4 Projects Status</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table project-tbl table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="40%">Grant</th>
                                                    <th width="55%">Progress</th>
                                                    <th width="5%">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Quran And Hadith Authentication Systems" class="black-tooltip">Project 1</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : CONTEXT-BASED KEYWORD PATTERN CLUSTER ANALYSIS TECHNIQUE ON TACIT KNOWLEDGE OF MILITARY TACTICS CENTER OF EXPERTISE" class="black-tooltip">Project 2</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Big Data And Mobile Cloud For Collaborative Experiments" class="black-tooltip">Project 3</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Computational History - Recreating The Past of Kuala Terengganu Port" class="black-tooltip">Project 4</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                        </div>
                                    </div><!-- / widget content -->
                                     <div class="panel-footer text-center text-size-small f-w-500">
                                        <small>This information is generated from Research Management System</small>
                                    </div>
                                </div><!-- / .panel -->
                            </div>  
                            <!-- End Widget Project -->
                            
                            <!-- Start Widget Collaborator -->
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default">
                                        <span>Collaborator</span>
                                    </div>
                                    <div class="panel-body p-b-10 p-t-10 text-center">
                                        
                                        <ul class="users-list clearfix">                                <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=rohanamahmud">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 1</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=wat">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 2</a>                                               
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=misslaiha">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 3</a>                                               
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=fariza">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 4</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=fazmidar">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 5</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=norisma">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 6</a>                                               
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=maizatul">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 7</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="#">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=sitihafizah">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="#">Collaborator 8</a>                                               
                                            </li>
                                        </ul>                           
                                    </div><!-- / widget content -->
                                    <div class="panel-footer text-center text-size-small f-w-500">
                                        <a href="#"><small>View all collaborators </small></a>                      </div>
                                </div><!-- / .panel -->
                            </div>  
                            <!-- End Widget Collaborator -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default ">
                                        <h3 class="panel-title">Latest Publication</h3> 
                                        <br class="visible-xs" />
                                        <span class="pull-right">
                                            <!-- Tabs -->
                                            <ul class="nav panel-tabs">
                                                <li class="active">
                                                <a href="#tab1" data-toggle="tab" title="Books">Books</a></li>
                                                <li>
                                                    <a href="#tab2" data-toggle="tab" title="Articles">Articles</a>
                                                </li>
                                                <li>
                                                    <a href="#tab3" data-toggle="tab" title="Book Chapters">Book Chapters</a>
                                                </li>
                                                <li>
                                                    <a href="#tab4" data-toggle="tab" title="Essays and Reviews">E&R</a>
                                                </li>
                                            </ul>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">

                                            <table class="table table-striped dataTable no-footer" id="list_of_pub_J" role="grid" aria-describedby="list_of_pub_J_info">
                                            <thead>
                                                <tr role="row" valign="top"><th class="sorting_disabled" rowspan="1" colspan="1" width="3%">NO</th><th class="sorting_disabled" rowspan="1" colspan="1">DETAILS OF BOOKS</th></tr>
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="even">
                                                    <td>1.</td>
                                                    <td>
                                                           Unequal Democracy: The Political Economy of the New Gilded Age, 2nd edition. New York: Russell Sage Foundation, and Princeton, NJ: Princeton University Press&nbsp;
                                                            <span class="label label-success">2016</span>
                                                        <!-- 
                                                        <div class="row m-0">Author(s) : <a href="abrizah">Prof. Dr. Abrizah Binti Abdullah</a></div>                       <br>
                                                                                    <div class="row m-0"><div class="col-sm-1 p-0">Source :</div><div>Learned Publishing</div></div> -->
                                                    </td>
                                                </tr>

                                                <tr role="row" class="odd">
                                                    <td>2.</td>
                                                    <td>
                                                            Democracy for Realists (with Christopher H. Achen). Princeton, NJ: Princeton University Press&nbsp;
                                                            <span class="label label-success">2016</span>
                                                        
                                                        <!-- <div class="row m-0">Author(s) : <a href="abrizah">Prof. Dr. Abrizah Binti Abdullah</a></div>                       <br>
                                                                                    <div class="row m-0"><div class="col-sm-1 p-0">Source :</div><div>Serials Review</div></div> -->
                                                    </td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td>3.</td>
                                                    <td>
                                                            Mass Politics in Tough Times: Opinions, Votes and Protest in the Great Recession (ed., with Nancy Bermeo). New York: Oxford University Press&nbsp;
                                                            <span class="label label-success">2014</span>
                                                        
                                                    </td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td>5.</td>
                                                    <td>
                                                            Unequal Democracy: The Political Economy of the New Gilded Age. New York: Russell Sage Foundation, and Princeton, NJ: Princeton University Press (2008). Subsequently published in Mandarin and Korean translations&nbsp;
                                                            <span class="label label-success">2014</span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="bottom">
                                                <div class="dataTables_info" style="text-align: center;">
                                                    <a href="#">View All</a>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab2">
                                                

                                                    <table class="table table-striped dataTable no-footer" id="list_of_pub_J" role="grid" aria-describedby="list_of_pub_J_info">
                                            <thead>
                                                <tr role="row" valign="top"><th class="sorting_disabled" rowspan="1" colspan="1" width="3%">NO</th><th class="sorting_disabled" rowspan="1" colspan="1">DETAILS OF ARTICLES</th></tr>
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="even">
                                                    <td>1.</td>
                                                    <td>
                                                          Failure to Converge: Presidential Candidates, Core Partisans, and the Missing Middle in American Electoral Politics.” ANNALS of the American Academy of Political and Social Science 667 (2016), 143-165&nbsp;
                                                            <span class="label label-success">2016</span>
                                                            <span class="label label-success">143-165</span>
                                                    </td>
                                                </tr>

                                                <tr role="row" class="odd">
                                                    <td>2.</td>
                                                    <td>
                                                            Remembering to Forget: A Note on the Duration of Campaign Advertising Effects. Political Communication 31:4 (2014), 532-544&nbsp;
                                                            <span class="label label-success">2014</span>
                                                            <span class="label label-success"> 532-544</span>
                                                        
                                                        <!-- <div class="row m-0">Author(s) : <a href="abrizah">Prof. Dr. Abrizah Binti Abdullah</a></div>                       <br>
                                                                                    <div class="row m-0"><div class="col-sm-1 p-0">Source :</div><div>Serials Review</div></div> -->
                                                    </td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td>3.</td>
                                                    <td>
                                                            A Generational Model of Political Learning” (with Simon Jackman). Electoral Studies 33:1 (2014), 7-18.&nbsp;
                                                            <span class="label label-success">2014</span>
                                                        
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="bottom">
                                                <div class="dataTables_info" style="text-align: center;">
                                                    <a href="#">View All</a>
                                                </div>
                                                </div>


                                            </div>
                                            <div class="tab-pane" id="tab3">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>
                                            <div class="tab-pane" id="tab4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

                                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
            
                            <!-- Start Widget Awards -->
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="padding-top: 5px;">
                                <div class="thumbnail text-center m-b-25" style="height: 410px;">
                                    <div class="panel-body widget-awards">
                                        <div class="widget-top">
                                            <span class="widget-title">Latest Award</span>
                                        </div>
                                        <div class="circle-container">
                                            <!-- Award icon -->
                                            <div id="awardstat" class="svg-container"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful">undefined<text text-anchor="middle" x="100" y="125" style="null" fill="#666"></text><circle cx="100" cy="100" r="57" class="border" fill="none" stroke="#eee" stroke-width="5" stroke-dasharray="360" transform="rotate(-90,100,100)"></circle><circle class="circle" cx="100" cy="100" r="57" fill="none" stroke="#02AFA5" stroke-width="5" stroke-dasharray="270, 20000" transform="rotate(-90,100,100)"></circle><text text-anchor="middle" x="100" y="124" class="icon" style="font-size: 62px" fill="#02AFA5"></text><text class="timer" text-anchor="middle" x="100" y="95" style="font-size: 22px; undefined;" fill="#aaa"></text></svg></div>
                                            <!-- /Award icon -->
                                        </div>
                                        <div class="caption caption-down">
                                            <span class="text-size-small f-w-500">
                                                Andrew Carnegie Fellowship from the Carnegie Corporation of New York, 2015-17

                                            </span>
                                        </div>
                                                             
                                    </div>                  
                                </div>
                            </div>
                            <!-- End Widget Awards -->
                            <!-- Start Widget Awards -->
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="padding-top: 5px;">
                                <div class="thumbnail text-center m-b-25" style="height: 410px;">
                                    <div class="panel-body widget-awards">
                                        <div class="widget-top">
                                            <span class="widget-title">Latest Award</span>
                                        </div>
                                        <div class="circle-container">
                                            <!-- Award icon -->
                                            <div id="awardstat" class="svg-container"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful">undefined<text text-anchor="middle" x="100" y="125" style="null" fill="#666"></text><circle cx="100" cy="100" r="57" class="border" fill="none" stroke="#eee" stroke-width="5" stroke-dasharray="360" transform="rotate(-90,100,100)"></circle><circle class="circle" cx="100" cy="100" r="57" fill="none" stroke="#02AFA5" stroke-width="5" stroke-dasharray="270, 20000" transform="rotate(-90,100,100)"></circle><text text-anchor="middle" x="100" y="124" class="icon" style="font-size: 62px" fill="#02AFA5"></text><text class="timer" text-anchor="middle" x="100" y="95" style="font-size: 22px; undefined;" fill="#aaa"></text></svg></div>
                                            <!-- /Award icon -->
                                        </div>
                                        <div class="caption caption-down">
                                            <span class="text-size-small f-w-500 text-justify">
                                            Warren E. Miller Prize
 from the
 Elections, Public Opinion, and Voting Behavior section 
of the A
merican P
olitical S
cience A
ssociation, 2014</span>
                                        </div>
                                                             
                                    </div>                  
                                </div>
                            </div>
                            <!-- End Widget Awards -->
                            <!-- Start Widget Activity -->
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="padding-top: 5px;">
                                <div class="thumbnail text-center m-b-25" style="height: 410px;">
                                    <div class="panel-body widget-activity">
                                        <div class="widget-top">
                                            <span class="widget-title">Latest Evaluation</span>
                                        </div>
                                        <div class="circle-container">
                                            <!-- Activity icon -->
                                            <div id="activitystat" class="svg-container"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful">undefined<text text-anchor="middle" x="100" y="125" style="null" fill="#666"></text><circle cx="100" cy="100" r="57" class="border" fill="none" stroke="#eee" stroke-width="5" stroke-dasharray="360" transform="rotate(-90,100,100)"></circle><circle class="circle" cx="100" cy="100" r="57" fill="none" stroke="#E295AE" stroke-width="5" stroke-dasharray="270, 20000" transform="rotate(-90,100,100)"></circle><text text-anchor="middle" x="100" y="124" class="icon" style="font-size: 62px" fill="#DF94AC"></text><text class="timer" text-anchor="middle" x="100" y="95" style="font-size: 22px; undefined;" fill="#aaa"></text></svg></div>
                                            <!-- /Activity icon -->
                                        </div>
                                                                    <div class="caption caption-down">
                                            <span class="text-size-small f-w-500">Elected to National Academy of Sciences</span>
                                        </div>
                                    </div>                  
                                </div>
                            </div>
                            <!-- End Widget Activity -->
                            <!-- Start Widget Activity -->
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="padding-top: 5px;">
                                <div class="thumbnail text-center m-b-25" style="height: 410px;">
                                    <div class="panel-body widget-activity">
                                        <div class="widget-top">
                                            <span class="widget-title">Latest Evaluation</span>
                                        </div>
                                        <div class="circle-container">
                                            <!-- Activity icon -->
                                            <div id="activitystat" class="svg-container"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful">undefined<text text-anchor="middle" x="100" y="125" style="null" fill="#666"></text><circle cx="100" cy="100" r="57" class="border" fill="none" stroke="#eee" stroke-width="5" stroke-dasharray="360" transform="rotate(-90,100,100)"></circle><circle class="circle" cx="100" cy="100" r="57" fill="none" stroke="#E295AE" stroke-width="5" stroke-dasharray="270, 20000" transform="rotate(-90,100,100)"></circle><text text-anchor="middle" x="100" y="124" class="icon" style="font-size: 62px" fill="#DF94AC"></text><text class="timer" text-anchor="middle" x="100" y="95" style="font-size: 22px; undefined;" fill="#aaa"></text></svg></div>
                                            <!-- /Activity icon -->
                                        </div>
                                                                    <div class="caption caption-down">
                                            <span class="text-size-small f-w-500">Grant from the John D. and Catherine T. MacArthur Foundation</span>
                                        </div>
                                    </div>                  
                                </div>
                            </div>
                            <!-- End Widget Activity -->
                            
                        </div>
                        <br/><br/>

                       <!--  <ul class="ul-boxed list-unstyled">
                            <li> Full Professor (tenure track 1996, tenure 1998)</li>
                            <li> Holder of Canada Research Chair Tier 1</li>
                            <li>Director, Synchromedia Laboratory and Consortium</li>
                            <li> Director, Green Star Network</li>
                        </ul>
                        <div class="title ">
                            <h3>HOME</h3>
                        </div>

                        <div class="col-md-12 ">
                            <p> Professor Cheriet has published more than 300 technical papers in renown international journals and conferences, and has delivered more than 20 invited talks. In addition, he has authored and published 6 books on pattern recognition, document image analysis and understanding, and computer vision. Among them, the book entitled Character Recognition Systems, a Textbook for Students and Practitioners, is highly acclaimed.</p>
                            <p> Prof. Cheriet is also recognized for his activities in technical journal editorial writing, organizing and taking part in many conferences. He has contributed to the training of more than 70 high qualified personnel. Dr. Cheriet was awarded the Queen Elizabeth II Diamond Jubilee Medal in light of his significant contributions to knowledge improvement in computational intelligence and mathematical modeling for image processing, created by MITCAS to mark the 60th anniversary of Her Majesty’s accession to the throne. He holds NSERC Canada Research Chair Tier 1 in Sustainable Smart echo-Cloud.</p>
                            <p> Dr. Cheriet is a senior member of the IEEE and the chapter founder and former chair of IEEE Montreal Computational Intelligent Systems (CIS).</p>
                        </div>   -->
                    </div>
                </div>
            </div>  
        </div>
    </body>
</html>